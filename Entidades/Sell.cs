﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Sell
    {
        public int sellId { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string place { get; set; }
        public string shipmenttime { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int products { get; set; }
        public int status { get; set; }
        public double total { get; set; }
        public int userId { get; set; }
    }
}
