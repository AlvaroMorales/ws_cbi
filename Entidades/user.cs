﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class user
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public int personId { get; set; }
        public string password { get; set; }
        public string description { get; set; }
        public int deleted { get; set; }
        public int insertedBy { get; set; }
        public string dateInserted { get; set; }
        public int updatedBy { get; set; }
        public string dateUpdated { get; set; }

        public user() { }

    }
}
