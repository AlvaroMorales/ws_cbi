﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Platform
    {
        int platformId { get; set; }
        string platform { get; set; }
        string description { get; set; }
        int deleted { get; set; }
        int insertedBy { get; set; }
        string dateInserted { get; set; }
        int updatedBy { get; set; }
        string dateUpdated { get; set; }

    }
}
