﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Product
    {
        public int productiId { get; set; }
        public string categoryId { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public double price { get; set; }
        public int stock { get; set; }
        public int deleted { get; set; }
        public int insertedBy { get; set; }
        public string dateInserted { get; set; }
        public int updatedBy { get; set; }
        public string dateUpdated { get; set; }
        public string image { get; set; }
    }
}
