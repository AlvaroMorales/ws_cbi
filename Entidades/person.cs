﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class person
    {
        public int personId { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public string birthDate { get; set; }

        public string imageUrl { get; set; }

        public int deleted{get;set;}
        public int insertedBy{get;set;}
        public string dateInserted{get;set;}
        public int updatedBy{get;set;}
        public string dateUpdated{get;set;}        
    }
}
