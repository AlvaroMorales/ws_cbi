﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Location
    {
        public int locationid { get; set; }
        public string name { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string description { get; set; }
        public int deleted { get; set; }
        public int insertedBy { get; set; }
        public string dateInserted { get; set; }
        public int updatedBy { get; set; }
        public string dateUpdated { get; set; }

    }
}
