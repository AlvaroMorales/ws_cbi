﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class SellObject
    {
       public int sellId { get; set; }
       public int status { get; set; }
       public int userId { get; set; }

       public SellObject() { 
       
       }
       ~SellObject() { 
       
       }

       public SellObject(int sellId, int status, int userId)
       {
           this.sellId = sellId;
           this.status = status;
           this.userId = userId;
       }


    }
}
