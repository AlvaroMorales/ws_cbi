﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public class ClientNegocio
    {
        ClientController clientController = new ClientController();
        user user = new user();


        public response saveClient(Client client)
        {
            response r = new response();
            try
            {
                r = clientController.saveClient(client).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }
        public List<Client> getClients()
        {
            return clientController.getClients();
        } 
    }
}
