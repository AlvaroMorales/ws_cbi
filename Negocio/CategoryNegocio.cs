﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public class CategoryNegocio
    {
        CategoryController categoryController = new CategoryController();
        user user = new user();
        public response saveCategory(Category cat)
        {
            response r = new response();
            try
            {
                r = categoryController.saveCategory(cat).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }
        public List<Category> getCategory()
        {
            return categoryController.getCategory();
        }
    }
}
