﻿using Datos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class SellNegocio
    {
        private SellController SellContrl = new SellController();

        public SellNegocio() { 
        
        }
        ~SellNegocio() { 
        
        }

        public List<Sell> GetSells(int id) {

            try {
                return SellContrl.getSell(id);
            }
            catch (Exception Ex) {
                throw Ex;
            }
        }


        public  response UpdateSell(SellObject sellObj) {
            response r = new response();
            try
            {
                r = SellContrl.updateSell(sellObj).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }

    }
}
