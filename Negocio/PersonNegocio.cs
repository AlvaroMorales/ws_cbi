﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;
namespace Negocio
{
    public class PersonNegocio
    {
        //METODOS DE PERSONA
        personController personController = new personController();

        public List<person> getPerson()
        {
            List<person> list = new List<person>();
            try
            {
                list = new personController().getPerson();
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
        public person getPersonById(int id)
        {
            person person = new person();
            try
            {
                person = personController.getPersonById(id);
                return person;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void savePerson(person p)
        {
            personController.savePerson(p);
        }

    }
}
