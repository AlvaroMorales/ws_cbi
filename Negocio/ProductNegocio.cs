﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public class ProductNegocio
    {
        ProductController productObj = new ProductController();
        public response saveProduct(Product cat)
        {
            response r = new response();
            try
            {
                r = productObj.saveProduct(cat).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }

        public response uploadImage(ImageByte Image)
        {
            response r = new response();
            try
            {
                r = productObj.uploadImage(Image).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }

        public List<Product> getProduct()
        {
            return productObj.getProduct();
        }
    }
}
