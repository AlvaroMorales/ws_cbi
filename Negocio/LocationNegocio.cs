﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public class LocationNegocio
    {
        Datos.LocationController locationObj = new Datos.LocationController();
        public response saveLocation(Entidades.Location location)
        {
            response r = new response();
            try
            {
                r = locationObj.saveLocation(location).Result;
            }
            catch (Exception)
            {
                throw;
            }
            return r;
        }
        public List<Entidades.Location> getLocation()
        {
            return locationObj.getLocation();
        }
        public Entidades.Location getLocationById(int id)
        {
            return locationObj.getLocationById(id);
        }


    }
}
