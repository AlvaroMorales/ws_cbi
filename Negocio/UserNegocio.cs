﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio
{
    public class UserNegocio
    {
        userController userController = new userController();
        user user = new user();
        public Entidades.userResponse getUser(string userName, string password,int platformId)
        {
            Entidades.userResponse userE = new userResponse();

            userE = userController.login(userName, password, platformId).Result;

            return userE;   
        }
        public List<Entidades.user> getUsers()
        {
            List<Entidades.user> user = new List<Entidades.user>();

            user = userController.getUsers();
            return user;
        }
        

    }
}
