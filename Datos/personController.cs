﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Entidades;

namespace Datos
{
    public class personController
    {
        HttpClient client = new Conexion().getConexion();
        person person = new person();

        public List<person> getPerson()
        {
            List<person> personList = new List<person>();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetPerson").Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;

                    personList = JsonConvert.DeserializeObject<List<person>>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return personList;
        }
        public person getPersonById(int id)
        {
            person person = new person();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetPersonById/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    person = JsonConvert.DeserializeObject<person>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return person;
        }
        public async Task<response> savePerson(person person)
        {
            response response = new response();
            try
            {
                var respuesta = client.PostAsJsonAsync("https://cbandasociates.xyz/API/Pruebas/api/SavePerson", person);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                if (respuesta.Result.StatusCode == HttpStatusCode.OK)
                {
                    // La respuesta es correcta y por ejemplo la retorno como string
                    string responseString = respuesta.Result.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<response>(responseString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + ex.Message);
            }
            return response;
        }
        
    }
}
