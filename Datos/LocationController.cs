﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Entidades;

namespace Datos
{
    public class LocationController
    {
        HttpClient client = new Conexion().getConexion();
        public async Task<response> saveLocation(Entidades.Location location)
        {
            response response = new response();
            try
            {
                var respuesta = client.PostAsJsonAsync("https://cbandasociates.xyz/API/Pruebas/api/SaveLocation", location);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                if (respuesta.Result.StatusCode == HttpStatusCode.OK)
                {
                    // La respuesta es correcta y por ejemplo la retorno como string
                    string responseString = respuesta.Result.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<response>(responseString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + ex.Message);
            }
            return response;
        }
        public List<Entidades.Location> getLocation()
        {
            List<Entidades.Location> location = new List<Entidades.Location>();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetLocation").Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    location = JsonConvert.DeserializeObject<List<Entidades.Location>>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return location;
        }
        public Entidades.Location getLocationById(int id)
        {
            Entidades.Location location = new Entidades.Location();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetLocationById/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    location = JsonConvert.DeserializeObject<Entidades.Location>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return location;
        }

    }
}
