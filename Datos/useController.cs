﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Newtonsoft.Json;

namespace Datos
{
    public class userController
    {
        HttpClient client = new Conexion().getConexion();
        user person = new user();

        public async Task<userResponse> login(string userName, string password, int platformId)
        {
            userResponse user = new userResponse();

            UserParams UserParameters = new UserParams
            {
                platformId = platformId,
                password = password,
                userName = userName
            };
            response response = new response();
            try
            {
                var respuesta = client.PostAsJsonAsync("https://cbandasociates.xyz/API/Pruebas/api/login", UserParameters);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                if (respuesta.Result.StatusCode == HttpStatusCode.OK)
                {
                    // La respuesta es correcta y por ejemplo la retorno como string
                    string personString = respuesta.Result.Content.ReadAsStringAsync().Result;
                    user = JsonConvert.DeserializeObject<userResponse>(personString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + ex.Message);
            }
            return user;
        }
        public List<Entidades.user> getUsers()
        {
            List<Entidades.user> user = new List<Entidades.user>();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetUsers").Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    user = JsonConvert.DeserializeObject<List<Entidades.user>>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return user;
        }
        public Entidades.user getUserById(int id)
        {
            Entidades.user user = new Entidades.user();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetUserById/"+id).Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    user = JsonConvert.DeserializeObject<Entidades.user>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return user;
        }
        
        public async void saveUser(user user)
        {
            string resp = "";
            response response = new response();
            try
            {
                var respuesta = client.PostAsJsonAsync("API/Php/person.php", user).Result;
                if (respuesta.StatusCode == HttpStatusCode.OK)
                {
                    // La respuesta es correcta y por ejemplo la retorno como string
                    resp = await respuesta.Content.ReadAsStringAsync();
                    response = JsonConvert.DeserializeObject<response>(resp);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
