﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Entidades;

namespace Datos
{
    public class CategoryController
    {
        HttpClient client = new Conexion().getConexion();
        public async Task<response> saveCategory(Category category)
        {
            response response = new response();
            try
            {
                var respuesta = client.PostAsJsonAsync("https://cbandasociates.xyz/API/Pruebas/api/SaveCategory", category);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;
                if (respuesta.Result.StatusCode == HttpStatusCode.OK)
                {
                    // La respuesta es correcta y por ejemplo la retorno como string
                    string responseString = respuesta.Result.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<response>(responseString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR " + ex.Message);
            }
            return response;
        }
        public List<Category> getCategory()
        {
            List<Category> category = new List<Category>();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetCategory").Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    category = JsonConvert.DeserializeObject<List<Category>>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return category;
        }
        public Category getCategoryById(int id)
        {
            Category category = new Category();
            try
            {
                string resultado = "";
                HttpResponseMessage response = new HttpResponseMessage();
                response = client.GetAsync("https://cbandasociates.xyz/API/Pruebas/api/GetCategoryById/"+id).Result;
                if (response.IsSuccessStatusCode)
                {
                    resultado = response.Content.ReadAsStringAsync().Result;
                    category = JsonConvert.DeserializeObject<Category>(resultado);
                }
                else
                {
                    resultado = response.IsSuccessStatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return category;
        }

    }
}
