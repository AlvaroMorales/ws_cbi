﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
using Entidades;
using System.Globalization;

namespace CBI_SystemWeb
{
    public partial class Person : System.Web.UI.Page
    {
        PersonNegocio personNegocio = new PersonNegocio();
        Utils utils = new Utils();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            fillData();
        }
        private void fillData()
        {
            gvTable.DataSource = dt();
            gvTable.DataBind();
            GridDataTable(gvTable);

        }
        public DataTable dt()
        {
            List<person> p = new List<person>();
            p = personNegocio.getPerson();

            return utils.ToDataTable(p);
        }
        private void GridDataTable(GridView Grid)
        {
            if (Grid.Rows.Count > 0)
            {
                Grid.UseAccessibleHeader = true;
                Grid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }


        //TODO: Mandar esto a una Utility

      /// <summary>
        ///  Obtiene una fecha en formato estandar (should be in a Utility method)
      /// </summary>
      /// <param name="DateFromText">La fecha (que seguramente viene de un textbox)</param>
      /// <returns>String</returns>
        private String getFormattedDate(String DateFromText) {

            try {
                DateTime CurrentDate = DateTime.ParseExact(DateFromText, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return CurrentDate.ToString("yyyyMMdd");

            }
            catch (Exception Ex) {
                throw Ex; 

            }
        }

        private String GetCurrentDate() {
            try
            {
                DateTime CurrentDate = DateTime.Now.Date;
                return CurrentDate.Day + "/" + CurrentDate.Month + "/" + CurrentDate.Year;

            }
            catch (Exception Ex)
            {
                throw Ex;

            }
        
        }
        protected void BtnGrabarAdminUser_Click(object sender, EventArgs e)
        {
            confirmDataPerson();
            confirmDataUser();
            person p = new person();
            p.lastName = txtApellido.Text;
            p.name = TxtNombresPersonaAdminUser.Text;
            p.gender = ddlSexo.SelectedValue;
            p.updatedBy = 1;
            p.insertedBy = 1;
            p.dateInserted = getFormattedDate(GetCurrentDate());
            p.dateUpdated = getFormattedDate(GetCurrentDate());
            p.birthDate = getFormattedDate(TxtFechaFinal.Text);
            personNegocio.savePerson(p);
        }
        public bool confirmDataPerson()
        {
            bool ret = true;
            if (TxtNombresPersonaAdminUser.Text.ToString().Equals("") || TxtDocumentoIdentidadAdminUser.Text.ToString().Equals("") || TxtNumeroTelefonoAdminUser.Text.ToString().Equals(""))
            {
                ret = false;
            }
            return ret;
        }
        public bool confirmDataUser()
        {
            bool ret = true;
            if (TxtNombreUsuarioAdminUser.Text.ToString().Equals("") || TxtClaveUsuarioConfirmAdminUser.Text.ToString().Equals("") || TxtClaveUsuarioAdminUser.Text.ToString().Equals("") || !TxtClaveUsuarioAdminUser.Text.ToString().Equals(TxtClaveUsuarioConfirmAdminUser.Text.ToString()))
            {
                ret = false;
            }
            return ret;
        }

    }
}