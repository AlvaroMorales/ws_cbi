﻿using Entidades;
using Negocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CBI_SystemWeb
{
    public partial class Sell : System.Web.UI.Page
    {
        private SellNegocio SellNeg = new SellNegocio();
        private UserNegocio UsrNegocio = new UserNegocio();

        private const String CSS_AMARILLO = "btn btn-warning";
        private const String CSS_VERDE = "btn btn-success";
        private const String CSS_AZUL = "btn btn-primary";
        private const String CSS_ROJO = "btn btn-danger";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                try {
                    BindUsersDrop();
                    BindGridView(int.Parse(DdlEstadosSell.SelectedValue));
                }
                catch (Exception Ex) {
                    MostrarError(Ex.Message);
                }
                
            }
        }
        public DataTable GetUsersAsDT() {
            try {
                List<Entidades.user> Users = UsrNegocio.getUsers();
                return new Utils().ToDataTable(Users);
            }
            catch (Exception Ex) {
                throw Ex;
            }
            
        }
        public DataTable GetSellsAsDT(int Status)
        {
            try {
                List<Entidades.Sell> Sells = SellNeg.GetSells(Status);
                return new Utils().ToDataTable(Sells);
            }
            catch (Exception Ex) {
                throw Ex;
            }
            
        }

        //TODO: Borrar esta wea
        private DataTable Foo() {
            DataTable Data = new DataTable();
            Data.Clear();
            Data.Columns.Add("Nombre");
            Data.Columns.Add("Descripcion");


            DataRow Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 45";
            Row["Descripcion"] = "Pedido a Nombre de Jaime Talero";
            Data.Rows.Add(Row);

            Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 46";
            Row["Descripcion"] = "Pedido a Nombre de Olga Seosa";
            Data.Rows.Add(Row);

            Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 47";
            Row["Descripcion"] = "Pedido a Nombre de Jorge Nitales";
            Data.Rows.Add(Row);

            Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 48";
            Row["Descripcion"] = "Pedido a Nombre de Juampi Cadura";
            Data.Rows.Add(Row);

            Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 49";
            Row["Descripcion"] = "Pedido a Nombre de Roberta Ladro";
            Data.Rows.Add(Row);

            Row = Data.NewRow();
            Row["Nombre"] = "Pedido # 50";
            Row["Descripcion"] = "Pedido a Nombre de Elvio Lento";
            Data.Rows.Add(Row);
            return Data;
        
        }

        private void BindGridView(int Status) {
            try {
                Button Buttoncillo;
                gvSell.DataSource = GetSellsAsDT(Status);
                gvSell.DataBind();

                if (gvSell.Rows.Count > 0)
                {
                    int CurrentItem = 0;
                    do
                    {
                        Buttoncillo = (Button)(gvSell.Rows[CurrentItem]).FindControl("btnAsignarSell");
                        switch (Status)
                        {
                            case -1:
                                Buttoncillo.Text = "Reintentar";
                                Buttoncillo.CssClass = CSS_ROJO;
                                break;
                            case 0:
                                Buttoncillo.Text = "Asignar";
                                Buttoncillo.CssClass = CSS_VERDE;
                                break;
                            case 1:
                                Buttoncillo.Text = "ReAsignar";
                                Buttoncillo.CssClass = CSS_AMARILLO;
                                break;
                            case 2:
                                Buttoncillo.Text = "Ver Detalles";
                                Buttoncillo.CssClass = CSS_AZUL;
                                break;
                            case 3:
                                Buttoncillo.Text = "Ver Compra";
                                Buttoncillo.CssClass = CSS_VERDE;
                                break;
                            default:
                                Buttoncillo.Text = "Asignar";
                                Buttoncillo.CssClass = CSS_VERDE;
                                break;
                        }
                        CurrentItem++;
                    } while (CurrentItem <= gvSell.Rows.Count - 1);



                }
                GridDataTable(gvSell);
            
            }
            catch (Exception Ex) {
                throw Ex;
            }
            
        }
        private void GridDataTable(GridView Grid)
        {
            try {
                if (Grid.Rows.Count > 0)
                {
                    Grid.UseAccessibleHeader = true;
                    Grid.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch (Exception Ex) {
                throw Ex;
            }

           
        }
        private void BindUsersDrop() {
            try {
                DdlAgentesSell.DataSource = GetUsersAsDT();
                DdlAgentesSell.DataTextField = "userName";
                DdlAgentesSell.DataValueField = "userId";
                DdlAgentesSell.DataBind();
            }
            catch (Exception Ex) {
                throw Ex;
            }
           
        
        }
        private void MostrarOk() {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert('Solicitud procesada correctamente');", true);
        }

        private void MostrarError(String Message) {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "erroralert('" + Message + "');", true);
        }
        protected void btnValidarCli_Click(object sender, EventArgs e)
        {
            try {
                int SellId = int.Parse(HiddenForPedido.Value);
                int Agent = int.Parse(DdlAgentesSell.SelectedValue);
                SellNeg.UpdateSell(new SellObject(SellId, 1, Agent));
                MostrarOk();
            }
            catch (Exception Ex) {
                MostrarError(Ex.Message);
            }
           
        }

        protected void DdlEstadosSell_SelectedIndexChanged(object sender, EventArgs e)
        {
            try {
                BindGridView(int.Parse(DdlEstadosSell.SelectedValue));
            }
            catch (Exception Ex) {
                MostrarError(Ex.Message);
            }
            
        }
    }
}