﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="CBI_SystemWeb.Product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        function openModalCategory() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");
            $('#idModalCategory').modal('show');
            return false;
        }

    </script>

    <div class="bs-callout bs-callout-warning container fuentemostrosa">
        
        <legend class="text-">
            <h1>
                <span class="fa fa-product-hunt"></span>roductos
            </h1>
        </legend>
        <br />
        <div class="row">
            <div class="col-sm-4">
                <label>Categoría</label>
                <asp:DropDownList runat="server" ID="ddlCategory" CssClass="form-control input-sm" AutoPostBack="True"></asp:DropDownList>
                <asp:LinkButton ID="btnAddCategory" runat="server" OnClientClick="return openModalCategory()">
                    <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                </asp:LinkButton>
            </div>
            <div class="col-sm-4">
                <label>Nombre</label>
                <asp:TextBox runat="server" ID="txtName" CssClass="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <label>Precio</label>
                <asp:TextBox runat="server" ID="txtPrice" CssClass="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <label>Stock</label>
                <asp:TextBox runat="server" ID="txtStock" CssClass="form-control input-sm"></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-6">
                <label>Seleccione una imagen</label>
                <asp:LinkButton runat="server" ID="idShowImage" OnClick="idShowImage_Click">
                            <i class="fa fa-eye fa-lg" aria-hidden="true"></i>
                </asp:LinkButton>
                <asp:FileUpload runat="server" ID="UploadFileInterm" accept=".jpg" ClientIDMode="Static" CssClass="form-control btn-default" />
            </div>

            <div class="col-sm-3">
                <asp:Image ImageUrl="imageUrl" ID="imageUrl" runat="server" CssClass="bs-callout-Image" />
            </div>
            <div class="col-sm-3">
                <label></label>
                <div class="form-group">
                    <asp:LinkButton runat="server" ID="btnUploadFileInterm" CssClass="btn btn-success btn-block" OnClick="btnUploadFileInterm_Click">
                        <i class="glyphicon glyphicon-upload"></i> Guardar Producto
                    </asp:LinkButton>
                </div>
                <%--</div>--%>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-sm-12">
                <asp:GridView runat="server" ID="GrdErroresCarga" CssClass="table table-responsive"
                    Caption="Errores" GridLines="None" HeaderStyle-CssClass="alert-danger" HeaderStyle-ForeColor="White">
                </asp:GridView>
            </div>
        </div>

        <asp:Panel runat="server" ID="pnResumen">
            <legend class="text-center text-success"><span class="fa fa-subway"></span><strong>Resumen de la carga</strong></legend>

            <asp:GridView runat="server" ID="gvProduct" CssClass="table table-responsive table-hover"
                HeaderStyle-CssClass="bg bg-primary" AutoGenerateColumns="true" GridLines="None">

            </asp:GridView>
        </asp:Panel>
        <div id="idModalCategory" class="modal fade in" role="dialog">
            <div class="modal-dialog modal-lg-12" style="width: 70%; align-content: center; text-align: center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h1 class="modal-title text-primary">
                                <asp:Label runat="server"   Text="Categoría" Font-Bold="true" Font-Size="X-Large" ID="lblCategoryTitle"></asp:Label>
                        </h1>
                    </div>
                    <div class="modal-body modal-centertext">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Categoría</label>
                                <asp:TextBox runat="server" ID="txtCategoryName" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-6">
                                <label>Descripción</label>
                                <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView runat="server" ID="gvCaregories" AutoGenerateColumns="true" CssClass="table" GridLines="None">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
