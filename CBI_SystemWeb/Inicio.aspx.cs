﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Collections.Generic;
using Entidades;
using Newtonsoft.Json;
using Negocio;
namespace CBI_SystemWeb
{
    public partial class Inicio : System.Web.UI.Page
    {
        userResponse user = new userResponse();
        UserNegocio userNegocio = new UserNegocio();
        PersonNegocio personNegocio = new PersonNegocio();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void LogIn(object sender, EventArgs e)
        {
            Session.Clear();

            if (!string.IsNullOrEmpty(txtUserName.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                try
                {
                    user = userNegocio.getUser(txtUserName.Text, txtPassword.Text, 1);
                    switch (user.Id)
                    {
                        
                        case -1:
                            lblmsg.Text = "¡Usuario no encontrado o no tiene permisos!";
                            lblmsg.CssClass = "alert alert-warning";
                            lblmsg.ForeColor = System.Drawing.Color.Red;

                            break;
                        case 0:
                            response response = (response)user.User;
                            lblmsg.Text = "¡" + response.Result + "!";

                            lblmsg.Text = "¡Usuario no encontrado o no tiene permisos!";
                            lblmsg.CssClass = "alert alert-warning";
                            lblmsg.ForeColor = System.Drawing.Color.Red;
                            break;
                        default:
                            Session["userName"] = txtUserName.Text;
                            Entidades.user test = JsonConvert.DeserializeObject<Entidades.user>(user.User.ToString());
                            person personUser = personNegocio.getPersonById(test.personId);
                            Session["name"] = personUser.name;

                            Response.Redirect("Dashboard.aspx");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    lblmsg.Text = "Error autenticando... " + ex.Message;
                    lblmsg.CssClass = "alert alert-warning";
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                lblmsg.Text = "¡Debe completar los campos!";
                lblmsg.CssClass = "alert alert-warning";
                lblmsg.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

}
