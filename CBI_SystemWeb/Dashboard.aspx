﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="CBI_SystemWeb.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Mes', 'Ventas'],
                ['Julio', 1800],
                ['Agosto', 6000],
                ['Septiembre', 4000],
                ['Octubre', 8000],
                ['Noviembre', 9100],
                ['Diciembre', 12000]
            ]);

            var options = {
                title: 'Ventas Create_Believe_And_Innovate',
                hAxis: { title: 'Mes', titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0 }
            };

            var chart = new google.visualization.LineChart(document.getElementById('pieSell'));
            chart.draw(data, options);
        }
        function openSell() {
            window.open("Sell.aspx", '_blank');
            return false;
        }
    </script>

    <link href="Resources/dashboard.css" rel="stylesheet" />
    <div class="container fuentemostrosa">
        <legend class="text-" style="text-align: center">
            <h2>Dashboard
            </h2>
        </legend>
        <p>
        </p>
        <div class="row">
            <!-- card 1-->
            <div class="col-md-4" runat="server" id="divIdPrincipal">
                <div class="card border-left-primary shadow h-100 py-2" runat="server" id="divIdPrincipal1" onclientclick="alert('Yep');">
                    <div href="#" class="card-body" style="text-align: center" runat="server" id="divIdPrincipal2" onclientclick="alert('Yep');">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Total de Ventas
                                </div>
                                <asp:Label Text="C$23,000" runat="server" Font-Size="Large" />

                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                </div>
                            </div>
                            <div class="col-auto">
                                <asp:LinkButton Text="text" runat="server" OnClientClick="return openReport('Principal');">
                                                <i class="fas fa-calendar fa-2x "></i>
                                </asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- card 2 -->
            <div class="col-md-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body" style="text-align: center">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Número de Ventas</div>
                                <asp:Label Text="87" runat="server" Font-Size="Large" />

                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800 mrleftper">
                                </div>
                            </div>
                            <div class="col-auto">
                                <asp:LinkButton Text="text" runat="server" OnClientClick="return openReport('Pendiente');">
                                            <i class="fas fa-clipboard-list fa-2x "></i>
                                </asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body" style="text-align: center">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pendintes</div>
                                <asp:Label Text="12" runat="server" Font-Size="Large" />
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800 mrleftper">
                                    <asp:Label ID="Label1" runat="server" />
                                </div>
                            </div>
                            <div class="col-auto">
                                <asp:LinkButton Text="text" runat="server" OnClientClick="return openReport('Pendiente');">
                                            <i class="fas fa-clipboard-list fa-2x "></i>
                                </asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary" style="padding: 5px">Ventas</h4>
                        <div class="dropdown no-arrow">
                            <div class="col-xl-12">
                                <asp:Button Text="Ver detalles" CssClass="btn btn-primary" OnClientClick="return openSell()" Style="margin: 5px" runat="server" />
                            </div>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" style="margin-left: 200px" aria-labelledby="dropdownMenuLink" onclick="event.stopPropagation();">
                                <div class="dropdown-header">Opciones:</div>
                                <a class="dropdown-item" href="#"></a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <div class="col-md-12">
                                <div id="pieSell" style="width: 1110px; height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>




