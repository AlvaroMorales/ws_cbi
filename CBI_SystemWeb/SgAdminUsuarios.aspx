﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SgAdminUsuarios.aspx.cs" Inherits="WebBolsagsa.SgAdminUsuarios" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#GrdUsuariosRegistrados").DataTable({
                language: {
                    "search": "Buscar:",
                    "Show ": "Mostrar",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>

    <div class="container">
        <h2 class="text-center text-info">- Listado de usuarios registrados -</h2>


        <div class="text-center">
            <asp:LinkButton runat="server" ID="BtnAgregarNuevo" CssClass="btn btn-success btn-sm" OnClick="BtnAgregarNuevo_Click">
            <i class="fa fa-user-circle-o"></i> Agregar nuevo usuario
            </asp:LinkButton>
        </div>
        <br />
        <div class="col-sm-12">
            <asp:GridView runat="server" ID="GrdUsuariosRegistrados" CssClass="table table-responsive"
                AutoGenerateColumns="false" GridLines="None" ClientIDMode="Static">
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" />
                    <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE PERSONA" />
                    <asp:BoundField DataField="USUARIO" HeaderText="NOMBRE USUARIO" />
                    <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" />
                    <asp:BoundField DataField="FECHA_ALTA" HeaderText="FECHA CREACION" />

                    <asp:TemplateField>
                        <ItemTemplate>

                           <div class="btn-group">
  <button type="button" class="btn btn-danger">Action</button>
  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
