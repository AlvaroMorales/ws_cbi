﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SgaAdminUsers.aspx.cs" Inherits="WebBolsagsa.SgaAdminUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class ="container fuentemostro">
        <legend class="text-center text-success">- AMINISTRACION DE CUENTAS DE USUARIO -</legend>
        <legend class="text-info">Datos Personales del usuario</legend>
        <div class="bs-callout bs-callout-info">
              <div class="row">
                   <div class="col-sm-4">
                ID Usuario
                <asp:TextBox runat="server" ID="TxtIdUsuarioAdminUser" CssClass="form-control input-sm"></asp:TextBox>
            </div>

            <div class="col-sm-8">
                Nombres de la persona
                <asp:TextBox runat="server" ID="TxtNombresPersonaAdminUser" CssClass="form-control input-sm"></asp:TextBox>
            </div>
              </div>

        <div class="row">

                 <div class="col-sm-4">
                Tipo de documento de identidad
                <asp:DropDownList runat="server" ID="DdlTipoDocumentoAdminUser" CssClass="form-control input-sm">
                    <asp:ListItem Value="">SELECCIONE ...</asp:ListItem>
                    <asp:ListItem Value="C">CEDULA</asp:ListItem>
                    <asp:ListItem Value="R">CEDULA RESIDENTE</asp:ListItem>
                </asp:DropDownList>
            </div>


            <div class="col-sm-4">
                Numero de documento de identidad
                <asp:TextBox runat="server" ID="TxtDocumentoIdentidadAdminUser" CssClass="form-control input-sm"></asp:TextBox>
            </div>

              <div class="col-sm-4">
                Numero de telefono
                <asp:TextBox runat="server" ID="TxtNumeroTelefonoAdminUser" CssClass="form-control input-sm"></asp:TextBox>
            </div>
        </div>
        </div>

       <legend class="text-primary">Informacion de la cuenta de usuario</legend>
          <div class="bs-callout bs-callout-primary">
              <div class="row">
                  <div class="col-sm-4">
                        Nombre de usuario
                       <asp:TextBox runat="server" ID="TxtNombreUsuarioAdminUser" CssClass="form-control input-sm"></asp:TextBox>
                  </div>

                   <div class="col-sm-4">
                         Contrasena
                       <asp:TextBox runat="server" ID="TxtClaveUsuarioAdminUser" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                  </div>

                    <div class="col-sm-4">
                         Repetir Contrasena
                       <asp:TextBox runat="server" ID="TxtClaveUsuarioConfirmAdminUser" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                  </div>
              </div>
              <div class="row">
                   <div class="col-sm-4">
                         Usuario Activo
                       <asp:DropDownList runat="server" ID="DdlActivoInactivoAdminUser" CssClass="form-control input-sm">
                            <asp:ListItem Value="">SELECCIONE ...</asp:ListItem>
                            <asp:ListItem Value="1">SI</asp:ListItem>
                            <asp:ListItem Value="0">NO</asp:ListItem>
                       </asp:DropDownList>
                  </div>

                   <div class="col-sm-4">
                         Motivo inactivacion
                       <asp:DropDownList runat="server" ID="DdlMotivInactAdminUser" CssClass="form-control input-sm">
                            <asp:ListItem Value="">SELECCIONE ...</asp:ListItem>
                            <asp:ListItem Value="">MOTIVO A</asp:ListItem>
                            <asp:ListItem Value="">MOTIVO B</asp:ListItem>
                       </asp:DropDownList>
                  </div>
              </div>
             
              </div>
         <hr />
               <div class="row">
                   <div class="col-sm-2">
                       <asp:LinkButton runat="server" ID="BtnGrabarAdminUser" CssClass="btn btn-success btn-block" OnClick="BtnGrabarAdminUser_Click">
                           <i class="fas fa-save"></i>  GRABAR INFORMACIÓN
                       </asp:LinkButton>
                   </div>

                   <div class="col-sm-2">
                       <asp:LinkButton runat="server" ID="BtnCancelarAdminUser" CssClass="btn btn-danger btn-block">
                           <i class="fas fa-remove"></i>  CANCELAR
                       </asp:LinkButton>
                   </div>
               </div>

           <hr />

            <div class="row">
                <div class="col-sm-12">
                    <asp:Label runat="server" ID="LblMensajes"></asp:Label>
                </div>
            </div>
        <hr />
        <asp:Label runat="server" ID="LblMensaje" Visible="false"></asp:Label>
    </div>
     
</asp:Content>
