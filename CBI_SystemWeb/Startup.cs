﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CBI_SystemWeb.Startup))]
namespace CBI_SystemWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
