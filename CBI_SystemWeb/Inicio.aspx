﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="CBI_SystemWeb.Inicio" EnableEventValidation="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Resources/dashboard.css" rel="stylesheet" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/fontawesome.min.css" rel="stylesheet" />
    <link href="Content/fontawesome-all.min.css" rel="stylesheet" />
    <link href="Content/v4-shims.min.css" rel="stylesheet" />
    <link href="Content/svg-with-js.min.css" rel="stylesheet" />
    <script src="Scripts/fontawesome/fontawesome.min.js"></script>
    <script src="Scripts/fontawesome/all.min.js"></script>

    <title></title>
</head>
<body class="body">

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 " style="text-align: center; margin-left: 260px; margin-right: 100px; padding-left: 50px; padding-right: 20px;">
                <div class="panel panelcito-default">
                    <div class="panel-heading">
                        <strong class=""><i class="fas fa-user-shield"></i>Bienvenido CBI-System</strong>
                    </div>
                    <div class="panel-body">
                        <form runat="server">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3  control-label" style="margin-top: 5px;">Usuario:</label>
                                <div class="col-sm-9">
                                    <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" placeholder="Usuario" required="" />
                                    <%--<input name="txtusuario" type="text" id="txtusuario" class="form-control" placeholder="Usuario" required=""/>--%>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label" style="margin-top: 5px; padding-left: 5px;">Contraseña:</label>
                                <div class="col-sm-9">
                                    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="form-control" placeholder="Contraseña" required="" />
                                    <%--<input name="txtpassword" type="password" id="txtpassword" class="form-control" placeholder="Contraseña" required=""/>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="form-group last">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <asp:Button Text="Aceptar" runat="server" OnClick="LogIn" CssClass="btn btn-success btn-sm" />
                                    <%--<input type="submit" name="btnaceptar" value="Aceptar" id="btnaceptar" class="btn btn-success btn-sm"/>--%>
                                    <asp:Button Text="Cancelar" runat="server" ID="btnCancelar" CssClass="btn btn-default btn-sm" />
                                    <%--<input type="submit" name="btncancelar" value="Cancelar" id="btncancelar" class="btn btn-default btn-sm"/>--%>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        TI <a href="https://www.facebook.com/Create-believe-and-innovate-110151393656468/" target="_blank" class="">Create Believe and Innovate</a>
                        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>

        </div>
    </div>

</body>
</html>
