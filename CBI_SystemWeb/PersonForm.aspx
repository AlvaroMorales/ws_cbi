﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Async="true" CodeBehind="PersonForm.aspx.cs" Inherits="CBI_SystemWeb.Person" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        
       



        $(document).ready(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $("#gvTable").DataTable({
                language: {
                    "search": "Buscar:",
                    "Show ": "Mostrar",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "Showing": "Mostrando"
                }
            });
        });
        function openModalPlatform() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");
            $('#idModalPlatform').modal('show');
            return false;
        }
    </script>
    <div class="container fuentemostrosa">

        <legend class="text-center text-warning">
            <h1>-Personas-
            </h1>
        </legend>
        <div id="Idelements">

            <legend class="text-info">Datos</legend>
            <div class="bs-callout bs-callout-default">
                <div class="row">
                    <div class="col-sm-3">
                        Nombre
                        <asp:TextBox runat="server" ID="TxtNombresPersonaAdminUser" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        Apellido
                        <asp:TextBox runat="server" ID="txtApellido" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        Número de teléfono
                    <asp:TextBox runat="server" ID="TxtNumeroTelefonoAdminUser" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        Sexo
                   
                    <asp:DropDownList runat="server" ID="ddlSexo" CssClass="form-control input-sm">
                        <asp:ListItem Value="-1">SELECCIONE ...</asp:ListItem>
                        <asp:ListItem Value="0">Femenino</asp:ListItem>
                        <asp:ListItem Value="1">Masculino</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-3">
                        Tipo de documento de identidad
                   
                    <asp:DropDownList runat="server" ID="DdlTipoDocumentoAdminUser" CssClass="form-control input-sm">
                        <asp:ListItem Value="-1">SELECCIONE ...</asp:ListItem>
                        <asp:ListItem Value="C">CEDULA</asp:ListItem>
                        <asp:ListItem Value="R">CEDULA RESIDENTE</asp:ListItem>
                    </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        Número de documento de identidad
                    <asp:TextBox runat="server" ID="TxtDocumentoIdentidadAdminUser" CssClass="form-control input-sm"></asp:TextBox>
                    </div>


                      <div class="form-group">
                            <label class="col-sm-4 control-label" for="TxtFechaFinal">Fecha Final </label>
                            <div class="col-sm-4">
                                <div class="input-group date" id="datetimepicker1">
                                    <asp:TextBox runat="server" ID="TxtFechaFinal" CssClass="form-control input-sm" placeholder="Fecha Fin"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <a id="iconEnd" class="fa fa-calendar"></a>
                                    </span>
                                </div>
                            </div>
                        </div>



                </div>
            </div>
            <legend class="text-primary">Información de la cuenta de usuario</legend>
            <div class="bs-callout bs-callout-default">
                <div class="row">
                    <div class="col-sm-4">
                        Nombre de usuario
                   
                        <asp:TextBox runat="server" ID="TxtNombreUsuarioAdminUser" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-sm-4">
                        Contrasena
                    <asp:TextBox runat="server" ID="TxtClaveUsuarioAdminUser" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                    </div>

                    <div class="col-sm-4">
                        Repetir Contrasena
                      
                    <asp:TextBox runat="server" ID="TxtClaveUsuarioConfirmAdminUser" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        Plataforma
                   
                        <asp:DropDownList runat="server" ID="DdlActivoInactivoAdminUser" CssClass="form-control input-sm">
                            <asp:ListItem Value="">SELECCIONE ...</asp:ListItem>
                            <asp:ListItem Value="1">Móvil</asp:ListItem>
                            <asp:ListItem Value="0">Sistema Web</asp:ListItem>
                        </asp:DropDownList>
                        <asp:LinkButton ID="btnAddPlatform" runat="server" OnClientClick="return openModalPlatform()">
                    <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                        </asp:LinkButton>
                    </div>

                    <div class="col-sm-4">
                        Rol
                        <asp:DropDownList runat="server" ID="DdlMotivInactAdminUser" CssClass="form-control input-sm">
                            <asp:ListItem Value="">Seleccione ...</asp:ListItem>
                            <asp:ListItem Value="">Gestor</asp:ListItem>
                            <asp:ListItem Value="">Administrador</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <hr />
            <div class="row">
                <div class="col-sm-2">
                    <asp:LinkButton runat="server" ID="BtnGrabarAdminUser" CssClass="btn btn-success btn-block" OnClick="BtnGrabarAdminUser_Click">
                           <i class="fa fa-save"></i>  GUARDAR PERSONA
                       </asp:LinkButton>
                </div>
                <div class="col-sm-2">
                    <asp:LinkButton runat="server" ID="BtnCancelarAdminUser" CssClass="btn btn-danger btn-block">
                           <i class="fa fa-remove"></i>  CANCELAR
                       </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:Label runat="server" ID="LblMensajes"></asp:Label>
            </div>
        </div>
        <hr />
        <asp:Label runat="server" ID="LblMensaje" Visible="false"></asp:Label>
        <div class="bs-callout bs-callout-warning">
            <asp:GridView runat="server" ID="gvTable" AutoGenerateColumns="true" CssClass="table" GridLines="None" ClientIDMode="Static">
            </asp:GridView>
        </div>
        <div class="row">
            <div id="idModalPlatform" class="modal fade in" role="dialog">
                <div class="modal-dialog modal-lg-12" style="width: 50%; align-content: center; text-align: center" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h1 class="modal-title text-primary">
                                <asp:Label runat="server" Text="Plataformas" Font-Bold="true" Font-Size="X-Large" ID="lblCategoryTitle"></asp:Label>
                            </h1>
                        </div>
                        <div class="modal-body modal-centertext">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Plataforma</label>
                                    <asp:TextBox runat="server" ID="txtCategoryName" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-6">
                                    <label>Descripción</label>
                                    <asp:TextBox runat="server" ID="TextBox3" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView runat="server" ID="gvPlatform" DataKeyNames="Rango_Morosidad,Principal,Clientes,Pendiente" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                        <Columns>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
