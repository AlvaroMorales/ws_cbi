﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SlCargaIntermedia.aspx.cs" Inherits="WebBolsagsa.SlCargaIntermedia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container">


        <legend class="text-center text-primary"><span class="fa fa-upload"></span><strong>Carga de archivos : Intermedia</strong></legend>
        <br />
        <div class="row">
            <div class="col-sm-3">
                <label>Año de cierre</label>
                <asp:TextBox runat="server" ID="txtAnnioCierreInterm" CssClass="form-control input-sm"></asp:TextBox>
            </div>

            <div class="col-sm-3">
                <label>Mes de cierre</label>
                <asp:DropDownList runat="server" ID="ddlMesCierreInterm" CssClass="form-control input-sm"></asp:DropDownList>
            </div>

            <div class="col-sm-3">
                <label>Producto</label>
                <asp:DropDownList runat="server" ID="ddlProductoInterm" CssClass="form-control input-sm" AutoPostBack="True"></asp:DropDownList>
            </div>

            <div class="col-sm-3">
                <label>Unidad de medida</label>
                <asp:DropDownList runat="server" ID="ddlUnidadMedidaInterm" CssClass="form-control input-sm"></asp:DropDownList>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <label>Cliente</label>
                <asp:DropDownList runat="server" ID="ddlClienteInterm" CssClass="form-control input-sm"></asp:DropDownList>
            </div>

            <div class="col-sm-6">
                <label>Archivo</label>
                <div class="custom-file" id="customFile" lang="es">
                    <asp:FileUpload runat="server" ID="UploadFileInterm" ClientIDMode="Static" CssClass="custom-file-input" />
                    <label class="custom-file-label" id="fuLabel" for="exampleInputFile">
                        Seleccione archivo
                    </label>
                </div>

            </div>
            <div class="col-sm-3">
                <label></label>
                <div class="form-group">
                    <asp:LinkButton runat="server" ID="btnUploadFileInterm" CssClass="btn btn-success btn-block">
                        <i class="glyphicon glyphicon-upload"></i> Cargar Archivo
                    </asp:LinkButton>
                </div>

            </div>

        </div>
        <br />
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <asp:Label runat="server" ID="lblMensaje"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:GridView runat="server" ID="GrdErroresCarga" CssClass="table table-responsive"
                    Caption="Errores" GridLines="None" HeaderStyle-CssClass="alert-danger" HeaderStyle-ForeColor="White">
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <asp:GridView runat="server" ID="GrdCargaIntermedia" CssClass="table table-responsive"
                    Caption="Datos Cargados" GridLines="None" HeaderStyle-CssClass="bg-primary">
                </asp:GridView>
            </div>
        </div>

        <asp:Panel runat="server" ID="pnResumen">
            <legend class="text-center text-success"><span class="fa fa-subway"></span><strong>Resumen de la carga</strong></legend>

            <asp:GridView runat="server" ID="GrdResumenCarga" CssClass="table table-responsive table-hover"
                HeaderStyle-CssClass="bg bg-primary" AutoGenerateColumns="false" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="DEPARTAMENTO" HeaderText="DEPARTAMENTO" />
                    <asp:BoundField DataField="CONTEO" HeaderText="CONTEO" />
                    <asp:BoundField DataField="CANTIDAD" HeaderText="CANTIDAD" />
                    <asp:BoundField DataField="PRECIO" HeaderText="PRECIO" DataFormatString="C$ {0:###,###,###.00}" />
                </Columns>

            </asp:GridView>
        </asp:Panel>

    </div>
</asp:Content>
