﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Negocio;
using System.Globalization;
using Entidades;
using System.Data;

namespace CBI_SystemWeb
{
    public partial class Product : System.Web.UI.Page
    {
        private ProductNegocio ProductoNegocio = new ProductNegocio();
        private CategoryNegocio categoryNegocio = new CategoryNegocio();
        Utils utils = new Utils();

        protected void Page_Load(object sender, EventArgs e)
        {
            confirm();
            if (!IsPostBack)
            {
                getCategories();
                fillData();
            }
        }

        //TODO: Mandar a Utility
        private String getFormattedDate(String DateFromText)
        {
            try
            {
                DateTime CurrentDate = DateTime.ParseExact(DateFromText, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return CurrentDate.ToString("yyyyMMdd");

            }
            catch (Exception Ex)
            {
                throw Ex;

            }
        }

        private String GetCurrentDate()
        {
            try
            {
                DateTime CurrentDate = DateTime.Now.Date;
                return CurrentDate.Day + "/" + CurrentDate.Month + "/" + CurrentDate.Year;

            }
            catch (Exception Ex)
            {
                throw Ex;

            }

        }
        private void fillData()
        {
            gvProduct.DataSource = dt();
            gvProduct.DataBind();
            GridDataTable(gvProduct);
        }
        public DataTable dt()
        {
            List<Entidades.Product> p = new List<Entidades.Product>();
            p = ProductoNegocio.getProduct();
            return utils.ToDataTable(p);
        }
        private void GridDataTable(GridView Grid)
        {
            if (Grid.Rows.Count > 0)
            {
                Grid.UseAccessibleHeader = true;
                Grid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        public bool confirm()
        {
            return true;
        }
        protected void btnUploadFileInterm_Click(object sender, EventArgs e)
        {
            if (verify())
            {
                try
                {
                    ImageByte imageByte = new ImageByte();
                    imageByte.imageByte = GetEncodedImage();
                    string imageUrl = ProductoNegocio.uploadImage(imageByte).Result;
                    
                    Entidades.Product Producto = new Entidades.Product();
                    Producto.productiId = 0;
                    Producto.image = imageUrl;
                    Producto.categoryId = ddlCategory.SelectedValue;
                    Producto.description = "Tasa M  ";
                    Producto.name = txtName.Text;
                    Producto.price = Convert.ToInt16(txtPrice.Text);
                    Producto.stock = Convert.ToInt16(txtStock.Text);
                    Producto.deleted = 0;
                    Producto.insertedBy = 1;
                    Producto.dateInserted = getFormattedDate(GetCurrentDate());
                    Producto.updatedBy = 1;
                    Producto.dateUpdated = getFormattedDate(GetCurrentDate());

                    ProductoNegocio.saveProduct(Producto);
                }
                catch (Exception Ex)
                {

                }
            }
            

        }
        private bool verify()
        {
            bool comp = true;
            if (txtName.Text.Equals("") || txtPrice.Text.Equals("") || txtStock.Text.Equals("") || ddlCategory.SelectedIndex < 0 )
            {
                comp = false;
            }
            return comp;
        }

        protected void idShowImage_Click(object sender, EventArgs e)
        {
            int sizeImageUploaded = UploadFileInterm.PostedFile.ContentLength;
            byte[] image = new byte[sizeImageUploaded];
            UploadFileInterm.PostedFile.InputStream.Read(image, 0, sizeImageUploaded);

            Bitmap imageBinary = new Bitmap(UploadFileInterm.PostedFile.InputStream);
            string imageDataUrl = "data:Imagen/png;base64," + Convert.ToBase64String(image);
            imageUrl.ImageUrl = imageDataUrl;
            imageUrl.Style.Add("widht", "120px");
            imageUrl.Style.Add("height", "120px");
        }
        private String GetEncodedImage()
        {
            try
            {
                int sizeImageUploaded = UploadFileInterm.PostedFile.ContentLength;
                byte[] image = new byte[sizeImageUploaded];
                UploadFileInterm.PostedFile.InputStream.Read(image, 0, sizeImageUploaded);

                Bitmap imageBinary = new Bitmap(UploadFileInterm.PostedFile.InputStream);
                return "data:Imagen/png;base64,"+Convert.ToBase64String(image);
                // imageUrl.ImageUrl = imageDataUrl;
                // imageUrl.Style.Add("widht", "120px");
                //imageUrl.Style.Add("height", "120px");
            }
            catch (Exception Ex)
            {
                throw Ex;

            }
        }
        private void getCategories()
        {
            List<Category> listCategory = categoryNegocio.getCategory();
            foreach (Category cat in listCategory)
            {
                ListItem item = new ListItem();
                item.Text = cat.name;
                item.Value = cat.categoryId.ToString();
                ddlCategory.Items.Add(item);
            }
        }
    }
}