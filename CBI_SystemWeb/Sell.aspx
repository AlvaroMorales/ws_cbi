﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sell.aspx.cs" Inherits="CBI_SystemWeb.Sell" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function successalert(message) {
            swal({
                title: 'Correcto',
                text: message,
                type: 'success'
            });
        }

        function erroralert(message) {
            swal({
                title: 'Ooops!',
                text: message,
                type: 'error'
            });
        }

        $(document).ready(function () {
            
            var pedidoValue = $("#HiddenForPedido");
            var lbComentario = $("#lbComentario");
            $('#gvSell  input[id*=btnAsignarSell]').click(function (e) {         
                //alert($(this).val());
                pedidoValue.val($(this).parent().siblings(":first").text());
                lbComentario.text(pedidoValue.val());
                $('#modalAsignacion').modal('show');
                e.stopImmediatePropagation();
                e.preventDefault();
            });



    
 



            $("#gvSell").DataTable({
                language: {
                    "search": "Buscar:",
                    "Show ": "Mostrar",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "Showing": "Mostrando"
                }
            });
        });
        function openModalPlatform() {
            $("#spnTitle").text("Hola");
            $("#spnMsg").text("Este es un msj");
            $('#idModalPlatform').modal('show');
            return false;
        }
    </script>
    <br />
    <br />
    <br />

    <asp:HiddenField runat="server" ID="HiddenForPedido" ClientIDMode="Static" />
    <div class="container fuentemostrosa">
        <div class="row">
            <div class="col-md-12">
                <legend class="text-primary text-center"> <span class="fa fa-buysellads"></span> <strong> - Gestión de Pedidos - </strong></legend>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <label>SELECCIONE UN ESTADO</label>
                <asp:DropDownList runat="server" id="DdlEstadosSell" CssClass="form-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="DdlEstadosSell_SelectedIndexChanged">
                    <asp:ListItem Value="0">SIN ASIGNAR</asp:ListItem>
                    <asp:ListItem Value="1"> ASIGNADO</asp:ListItem>
                    <asp:ListItem Value="2">EN PROGRESO </asp:ListItem>
                    <asp:ListItem Value="3">COMPRA REALIZADA</asp:ListItem>
                    <asp:ListItem Value="-1">HUBO UN ERROR</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="bs-callout bs-callout-default">
            <div class="row">
   
            <div class="col-md-12">
                <asp:GridView runat="server" ID="gvSell" HeaderStyle-CssClass="alert alert-info" 
                    ClientIDMode="Static" AutoGenerateColumns="false" CssClass="table table-responsive" GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="sellId" HeaderText="sellId" />
                        <asp:BoundField DataField="name" HeaderText="name" />
                        <asp:BoundField DataField="lastName" HeaderText="lastName" />
                        <asp:BoundField DataField="place" HeaderText="place" />
                        <asp:BoundField DataField="shipmenttime" HeaderText="shipmenttime" />
                        <asp:BoundField DataField="latitude" HeaderText="latitude" />
                        <asp:BoundField DataField="longitude" HeaderText="longitude" />
                        <asp:BoundField DataField="products" HeaderText="products" />
                        <asp:BoundField DataField="status" HeaderText="status" />
                        <asp:BoundField DataField="total" HeaderText="total" />
                        <asp:BoundField DataField="userId" HeaderText="userId" />

                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button runat="server" ID="btnAsignarSell" ClientIDMode="Static" Text="ASIGNAR" CssClass="btn btn-success" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
     
            </div>

        </div>
        





             <div id='modalAsignacion' class='modal' tabindex='-1' role='dialog' aria-labelledby='modalAsignacion' aria-hidden='true'>
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: #FFFFFF;">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title">Asignar Pedido  <asp:Label runat="server" ID="lblNumeroPedido" ClientIDMode="Static"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                       <span class="fa fa-user-circle-o"></span> <strong>Seleccione Usuario para asignar pedido # <asp:Label runat="server" ID="lbComentario" ClientIDMode="Static"></asp:Label> </strong>
                                    </div>
                                </div>


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:DropDownList runat="server" ID="DdlAgentesSell" CssClass="form-control input-sm">
                                           <asp:ListItem Value="0"> SELECCIONE ...</asp:ListItem>
                                           <asp:ListItem Value="1"> AGENTE 1</asp:ListItem>
                                           <asp:ListItem Value="2"> AGENTE 4</asp:ListItem>
                                           <asp:ListItem Value="3"> AGENTE 8</asp:ListItem>
                                       </asp:DropDownList>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-sm-6 text-center">
                                        <asp:LinkButton runat="server" ID="btnValidarCli" CssClass="btn btn-primary btn-block" ClientIDMode="Static" OnClick="btnValidarCli_Click">
                            <i class="fa fa-check"></i> CONFIRMAR ASIGNACION
                                        </asp:LinkButton>
                                    </div>


                                    <div class="col-sm-6 text-center">
                                        <asp:LinkButton runat="server" ID="BtncancelarSell" CssClass="btn btn-danger btn-block" ClientIDMode="Static">
                            <i class="fa fa-trash-o"></i> CANCELAR 
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

</asp:Content>
